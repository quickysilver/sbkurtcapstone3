package ffufm.kurt.api.spec.handler.employee.implementation

import ffufm.kurt.api.PassTestBase
import ffufm.kurt.api.spec.handler.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals

class EmployeeContactDatabaseHandlerTest : PassTestBase() {

    @Test
    fun `create should return address`() = runBlocking {
        assertEquals(0, employeeContactRepository.findAll().count())
        val employee = employeeEmployeeRepository.save(EntityGenerator.createEmployee())
        val contactDetail = EntityGenerator.createContacts().toDto()
        val createdContactDetail = employeeContactDatabaseHandler.createContacts(
            contactDetail, employee.id!!
        )
        assertEquals(1, employeeContactRepository.findAll().count())
        assertEquals(contactDetail.contactDetail, createdContactDetail.contactDetail)
        assertEquals(contactDetail.contactType, createdContactDetail.contactType)
    }

    @Test
    fun `test deleteContacts should work`() = runBlocking {
        val employee = employeeEmployeeRepository.save(EntityGenerator.createEmployee())
        val contact = employeeContactRepository.save(
            EntityGenerator.createContacts().copy( employee = employee ))

        employeeContactDatabaseHandler.deleteContacts(contact.id!!)

        assertEquals(0, employeeContactRepository.findAll().count())
    }

    @Test
    fun `test updateContacts should return updated contacts`() = runBlocking {
        val employee = employeeEmployeeRepository.save(EntityGenerator.createEmployee())
        val contact = employeeContactRepository.save(
            EntityGenerator.createContacts().copy( employee = employee ))

        val body = contact.copy(
            contactDetail = "09182575936",
            contactType = "Phone Number",).toDto()

        val updatedContactDetail = employeeContactDatabaseHandler.updateContacts(body, contact.id!!)
        assertEquals(body.contactDetail, updatedContactDetail.contactDetail)
        assertEquals(body.contactType, updatedContactDetail.contactType)
    }

}
