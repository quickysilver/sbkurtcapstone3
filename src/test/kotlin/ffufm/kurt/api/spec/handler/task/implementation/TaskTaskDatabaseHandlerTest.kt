package ffufm.kurt.api.spec.handler.task.implementation

import ffufm.kurt.api.PassTestBase
import ffufm.kurt.api.repositories.task.TaskTaskRepository
import ffufm.kurt.api.spec.dbo.task.TaskTask
import ffufm.kurt.api.spec.handler.task.TaskTaskDatabaseHandler
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired

class TaskTaskDatabaseHandlerTest : PassTestBase() {
    @Autowired
    lateinit var taskTaskRepository: TaskTaskRepository

    @Autowired
    lateinit var taskTaskDatabaseHandler: TaskTaskDatabaseHandler
//
//    @Before
//    @After
//    fun cleanRepositories() {
//        taskTaskRepository.deleteAll()
//    }
//
//    @Test
//    fun `test createTasks`() = runBlocking {
//        taskTaskDatabaseHandler.createTasks()
//        Unit
//    }
//
//    @Test
//    fun `test remove`() = runBlocking {
//        val id: Long = 0
//        taskTaskDatabaseHandler.remove(id)
//        Unit
//    }
//
//    @Test
//    fun `test update`() = runBlocking {
//        val body: TaskTask = TaskTask()
//        val id: Long = 0
//        taskTaskDatabaseHandler.update(body, id)
//        Unit
//    }
}
