package ffufm.kurt.api.spec.handler.leaverecord.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.kurt.api.PassTestBase
import ffufm.kurt.api.repositories.leaverecord.LeaverecordLeaveRecordRepository
import ffufm.kurt.api.spec.dbo.leaverecord.LeaverecordLeaveRecord
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class LeaverecordLeaveRecordHandlerTest : PassTestBase() {
    @Autowired
    private lateinit var leaverecordLeaveRecordRepository: LeaverecordLeaveRecordRepository

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc

//    @Before
//    @After
//    fun cleanRepositories() {
//        leaverecordLeaveRecordRepository.deleteAll()
//    }
//
//    @Test
//    @WithMockUser
//    fun `test createleaveRecords`() {
//        val body: LeaverecordLeaveRecord = LeaverecordLeaveRecord()
//                mockMvc.post("/employees/{id}/leave-records/") {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//                    content = objectMapper.writeValueAsString(body)
//                }.andExpect {
//                    status { isOk }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test getByEmployee`() {
//        val id: Long = 0
//                mockMvc.get("/employees/{id}/leaverecords/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test getById`() {
//        val id: Long = 0
//                mockMvc.get("/leaverecords/{id}/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test update`() {
//        val body: LeaverecordLeaveRecord = LeaverecordLeaveRecord()
//        val id: Long = 0
//                mockMvc.put("/leaverecords/{id}/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//                    content = objectMapper.writeValueAsString(body)
//                }.andExpect {
//                    status { isOk }
//
//                }
//    }
}
