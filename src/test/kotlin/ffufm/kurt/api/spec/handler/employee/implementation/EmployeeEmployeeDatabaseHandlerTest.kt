package ffufm.kurt.api.spec.handler.employee.implementation

import ffufm.kurt.api.PassTestBase
import ffufm.kurt.api.spec.dbo.employee.EmployeeEmployee
import ffufm.kurt.api.spec.handler.utils.EntityGenerator
import ffufm.kurt.api.utils.UserTypeEnum
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.springframework.web.server.ResponseStatusException
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith


class EmployeeEmployeeDatabaseHandlerTest : PassTestBase() {


    @Test
    fun `test createEmployee should return an employee`() = runBlocking {
        assertEquals(0, employeeEmployeeRepository.findAll().count())
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeEmployeeDatabaseHandler.create(employee.toDto())

        assertEquals(1, employeeEmployeeRepository.findAll().count())
        assertEquals(employee.firstName, createdEmployee.firstName)
        assertEquals(employee.lastName, createdEmployee.lastName)
        assertEquals(employee.email, createdEmployee.email)
        assertEquals(employee.position, createdEmployee.position)
        assertEquals(employee.userType, createdEmployee.userType)
    }
    @Test
    fun `create duplicate email should fail `() = runBlocking {
        val employee = employeeEmployeeRepository.save(EntityGenerator.createEmployee())
        val duplicateUser = employee.copy(
            email = "greg.nue@f-bootcamp.com").toDto()
        val exception = assertFailsWith<ResponseStatusException> {
            employeeEmployeeDatabaseHandler.create(duplicateUser)
        }
        val expectedException = "409 CONFLICT \"Email ${duplicateUser.email} was already exists!\""
        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `test getAll employees should return all of the employees`() = runBlocking {
        val body = EntityGenerator.createEmployee()
        employeeEmployeeRepository.saveAll(
            listOf(body, body.copy(email = "greg.nue@f-bootcamp.com"))
        )
        val employees = employeeEmployeeDatabaseHandler.getAll(0, 100)
        assertEquals(2, employees.count())
    }

    @Test
    fun `test getById of employee should return an employee`() = runBlocking {
        val employee = EmployeeEmployee (
            firstName = "Greg",
            lastName = "Nue",
            email = "greg.neu@f-bootcamp.com",
            position = "Monteur",
            userType = UserTypeEnum.Employee.value
    )
        val createdEmployee = employeeEmployeeRepository.save(employee)

        employeeEmployeeDatabaseHandler.getById(createdEmployee.id!!)
        assertEquals(1, employeeEmployeeRepository.findAll().size)
    }
    @Test
    fun `update employee should return updated employee`() = runBlocking {
        val employee = EntityGenerator.createEmployee()
        val original = employeeEmployeeRepository.save(employee)

        val body = original.copy(
            firstName = "Greg,",
            lastName = "Nue",
            email = "greg.neu@google.com",
            position = "Monteur"
        )

        val updatedEmployee = employeeEmployeeDatabaseHandler.update(body.toDto(), original.id!!)
        assertEquals(body.firstName, updatedEmployee.firstName)
        assertEquals(body.lastName, updatedEmployee.lastName)
        assertEquals(body.email, updatedEmployee.email)
    }

    @Test
    fun `test remove employee should work`() = runBlocking {
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeEmployeeRepository.save(employee)

        assertEquals(1, employeeEmployeeRepository.findAll().count())
        employeeEmployeeDatabaseHandler.remove(createdEmployee.id!!)
        assertEquals(0, employeeEmployeeRepository.findAll().count())
    }


    @Test
    fun `test update employee should return updated employee`() = runBlocking {
        val employee = EntityGenerator.createEmployee()
        val original = employeeEmployeeRepository.save(employee)

        val body = original.copy(
            firstName = "Greg,",
            lastName = "Nue",
            email = "greg.neu@f-bootcamp.com",
            position = "Monteur"
        )

        val updatedEmployee = employeeEmployeeDatabaseHandler.update(body.toDto(), original.id!!)
        assertEquals(body.firstName, updatedEmployee.firstName)
        assertEquals(body.lastName, updatedEmployee.lastName)
        assertEquals(body.email, updatedEmployee.email)
    }
}
