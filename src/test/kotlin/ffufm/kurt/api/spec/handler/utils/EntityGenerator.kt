package ffufm.kurt.api.spec.handler.utils

import ffufm.kurt.api.spec.dbo.employee.EmployeeAddress
import ffufm.kurt.api.spec.dbo.employee.EmployeeContact
import ffufm.kurt.api.spec.dbo.employee.EmployeeEmployee
import ffufm.kurt.api.spec.dbo.project.ProjectProject
import ffufm.kurt.api.utils.UserTypeEnum

object EntityGenerator {
    fun createEmployee(): EmployeeEmployee = EmployeeEmployee(
        firstName = "Greg",
        lastName = "Nue",
        email = "greg.neu@f-bootcamp.com",
        position = "Monteur",
        userType = UserTypeEnum.Employee.value
    )

    fun createContacts(): EmployeeContact = EmployeeContact(
        contactDetail = "+49 1234 56 789 01",
        contactType = "Telephone Number"
    )

    fun createAddress(): EmployeeAddress = EmployeeAddress(
        street = "6783 Ayala Ave.",
        city = "Manila"
    )

    fun createProject(): ProjectProject = ProjectProject(
        name = "Time Tracker",
        description = "For adding new time tracker",
        status = "ON-GOING"
    )




}