package ffufm.kurt.api.spec.handler.project.implementation

import ffufm.kurt.api.PassTestBase
import ffufm.kurt.api.repositories.project.ProjectProjectRepository
import ffufm.kurt.api.spec.dbo.project.ProjectProject
import ffufm.kurt.api.spec.handler.project.ProjectProjectDatabaseHandler
import ffufm.kurt.api.spec.handler.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import kotlin.test.assertEquals

class ProjectProjectDatabaseHandlerTest : PassTestBase() {

    @Test
    fun `test createProject should return a project`() = runBlocking {
        val employee = employeeEmployeeRepository.save(EntityGenerator.createEmployee())
        val body = EntityGenerator.createProject().toDto()

        val createdProject = projectProjectDatabaseHandler.create(body, employee.id!!)
        assertEquals(1, projectProjectRepository.findAll().count())
        assertEquals(body.name, createdProject.name)
        assertEquals(body.description, createdProject.description)
        assertEquals(body.status, createdProject.status)
    }

    @Test
    fun `test getAllProjects should return all of the projects`() = runBlocking {
        val employee = employeeEmployeeRepository.save(EntityGenerator.createEmployee())
        val project =  EntityGenerator.createProject().copy( employee = employee )
        val maxResults = 100
        val page = 0

        projectProjectRepository.saveAll(
            listOf(
                project,
                project.copy( status = "ON-GOING")
            )
        )
        val users = projectProjectDatabaseHandler.getAll(maxResults, page)
        assertEquals(2, users.count())
    }

    @Test
    fun `test removeProject should work`() = runBlocking {
        val project = EntityGenerator.createProject()
        val createdProject = projectProjectRepository.save(project)

        assertEquals(1, projectProjectRepository.findAll().count())
        projectProjectDatabaseHandler.remove(createdProject.id!!)
        assertEquals(0, employeeEmployeeRepository.findAll().count())
    }

    @Test
    fun `test updateProject should return updated project`() = runBlocking {
        val project = EntityGenerator.createProject()
        val original = projectProjectRepository.save(project)
        val body = original.copy(
            name = "Time Tracker",
            description = "For adding new time tracker",
            status = "COMPLETED"
        ).toDto()

        val updatedProject = projectProjectDatabaseHandler.update(
            body, original.id!!)
        assertEquals(body.name, updatedProject.name)
        assertEquals(body.description, updatedProject.description)
        assertEquals(body.status, updatedProject.status)
    }
}
