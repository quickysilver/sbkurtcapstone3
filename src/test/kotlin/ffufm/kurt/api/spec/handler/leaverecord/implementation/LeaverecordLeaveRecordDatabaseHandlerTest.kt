package ffufm.kurt.api.spec.handler.leaverecord.implementation

import ffufm.kurt.api.PassTestBase
import ffufm.kurt.api.repositories.leaverecord.LeaverecordLeaveRecordRepository
import ffufm.kurt.api.spec.dbo.leaverecord.LeaverecordLeaveRecord
import ffufm.kurt.api.spec.handler.leaverecord.LeaverecordLeaveRecordDatabaseHandler
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired

class LeaverecordLeaveRecordDatabaseHandlerTest : PassTestBase() {
    @Autowired
    lateinit var leaverecordLeaveRecordRepository: LeaverecordLeaveRecordRepository

    @Autowired
    lateinit var leaverecordLeaveRecordDatabaseHandler: LeaverecordLeaveRecordDatabaseHandler
//
//    @Before
//    @After
//    fun cleanRepositories() {
//        leaverecordLeaveRecordRepository.deleteAll()
//    }
//
//    @Test
//    fun `test createleaveRecords`() = runBlocking {
//        val body: LeaverecordLeaveRecord = LeaverecordLeaveRecord()
//        leaverecordLeaveRecordDatabaseHandler.createleaveRecords(body)
//        Unit
//    }
//
//    @Test
//    fun `test getByEmployee`() = runBlocking {
//        val id: Long = 0
//        val maxResults: Int = 100
//        val page: Int = 0
//        leaverecordLeaveRecordDatabaseHandler.getByEmployee(id, maxResults, page)
//        Unit
//    }
//
//    @Test
//    fun `test getById`() = runBlocking {
//        val id: Long = 0
//        leaverecordLeaveRecordDatabaseHandler.getById(id)
//        Unit
//    }
//
//    @Test
//    fun `test update`() = runBlocking {
//        val body: LeaverecordLeaveRecord = LeaverecordLeaveRecord()
//        val id: Long = 0
//        leaverecordLeaveRecordDatabaseHandler.update(body, id)
//        Unit
//    }
}
