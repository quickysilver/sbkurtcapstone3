package ffufm.kurt.api.spec.handler.employee.implementation

import ffufm.kurt.api.PassTestBase

import ffufm.kurt.api.spec.handler.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.Test
import kotlin.test.assertEquals

class EmployeeAddressDatabaseHandlerTest : PassTestBase() {

    @Test
    fun `test createAddress should return an address`() = runBlocking {
        val employee = employeeEmployeeRepository.save(EntityGenerator.createEmployee())
        val address = EntityGenerator.createAddress().toDto()
        val createdAddress = employeeAddressDatabaseHandler.create(address, employee.id!!)

        assertEquals(createdAddress.street, address.street)
        assertEquals(createdAddress.city, address.city)
        assertEquals(1, employeeAddressRepository.findAll().count())
    }

    @Test
    fun `test removeAddress should work`() = runBlocking {
        val employee = employeeEmployeeRepository.save(EntityGenerator.createEmployee())
        val address = employeeAddressRepository.save(
            EntityGenerator.createAddress().copy( employee = employee ))

        employeeAddressDatabaseHandler.remove(address.id!!)

        assertEquals(0, employeeAddressRepository.findAll().count())
    }

    @Test
    fun `test updateAddress should return updated address`() = runBlocking {
        val employee = employeeEmployeeRepository.save(EntityGenerator.createEmployee())
        val address = employeeAddressRepository.save(
            EntityGenerator.createAddress().copy( employee = employee ))

        val body = address.copy(
            city = "Zamboanga"
        )
        val updatedAddress = employeeAddressDatabaseHandler.update(body.toDto(), address.id!!)
        assertEquals(body.city, updatedAddress.city)
    }
}
