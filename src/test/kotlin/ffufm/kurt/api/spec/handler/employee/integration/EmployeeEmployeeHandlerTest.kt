package ffufm.kurt.api.spec.handler.employee.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.kurt.api.PassTestBase
import ffufm.kurt.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.kurt.api.spec.dbo.employee.EmployeeEmployee
import ffufm.kurt.api.spec.handler.utils.EntityGenerator
import ffufm.kurt.api.utils.UserTypeEnum
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class EmployeeEmployeeHandlerTest : PassTestBase() {

    @Test
    @WithMockUser
    fun `test createEmployee should return isOk`() {
        val employee = EntityGenerator.createEmployee()
                mockMvc.post("/employees/") {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(employee)
                }.andExpect {
                    status { isOk() }

                }
    }

    @Test
    @WithMockUser
    fun `test getAllEmployees should return all of the employees`() {
                mockMvc.get("/employees/") {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON

                }.andExpect {
                    status { isOk() }

                }
    }

    @Test
    @WithMockUser
    fun `test getEmployeeById should return isOk`() {
            val body = EntityGenerator.createEmployee()
            val employee = employeeEmployeeRepository.save(body)
            mockMvc.get("/employees/{id}/", employee.id!!) {
                accept(MediaType.APPLICATION_JSON)
                contentType = MediaType.APPLICATION_JSON
            }.asyncDispatch().andExpect {
                status { isOk() }
            }
        }


        @Test
        @WithMockUser
        fun `test removeEmployee should return isOk`() {
            val body = EntityGenerator.createEmployee()
            val employee = employeeEmployeeRepository.save(body)
            mockMvc.delete("/employees/{id}/", employee.id!!) {
                accept(MediaType.APPLICATION_JSON)
                contentType = MediaType.APPLICATION_JSON
            }.asyncDispatch().andExpect {
                status { isOk() }
            }
        }

        @Test
        @WithMockUser
        fun `test updateEmployee should return isOk`() {
            val body = EntityGenerator.createEmployee()

            val savedEmployee = employeeEmployeeRepository.save(body)

            val updatedEmployee = savedEmployee.copy(
                firstName = "Greg",
                lastName = "Nue",
                email = "greg.neu@f-bootcamp.com",
                position = "Monteur",
                userType = UserTypeEnum.Employee.value
            )
            mockMvc.put("/employees/{id}/", body.id!!) {
                accept(MediaType.APPLICATION_JSON)
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(updatedEmployee)
            }.asyncDispatch().andExpect {
                status { isOk() }
            }
        }
}
