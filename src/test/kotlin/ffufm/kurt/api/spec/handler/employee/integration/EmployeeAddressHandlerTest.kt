package ffufm.kurt.api.spec.handler.employee.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.kurt.api.PassTestBase
import ffufm.kurt.api.repositories.employee.EmployeeAddressRepository
import ffufm.kurt.api.spec.dbo.employee.EmployeeAddress
import ffufm.kurt.api.spec.handler.utils.EntityGenerator
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class EmployeeAddressHandlerTest : PassTestBase() {

    @Test
    @WithMockUser
    fun `test createAddress should return isOk`() {
        val employee = employeeEmployeeRepository.save(EntityGenerator.createEmployee())
        val body = EntityGenerator.createAddress().copy(
            employee = employee
        )
        mockMvc.post("/employees/{id}/addresses/", employee.id!!) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.asyncDispatch().andExpect {
            status { isOk() }
        }
    }

    @Test
    @WithMockUser
    fun `test removeAddress should return isOk`() {
        val employee = employeeEmployeeRepository.save(EntityGenerator.createEmployee())
        val body = employeeAddressRepository.save(EntityGenerator.createAddress().copy(
            employee = employee
        ))

                mockMvc.delete("/employees/addresses/{id}/",body.id!!) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                }.asyncDispatch().andExpect {
                    status { isOk() }
                }
    }

    @Test
    @WithMockUser
    fun `test updateAddress should return isOk`() {
        val employee = employeeEmployeeRepository.save(EntityGenerator.createEmployee())
        val address = employeeAddressRepository.save(
            EntityGenerator.createAddress().copy(
                employee = employee
            ))
        val body = address.copy(
            street = "6783 Ayala Ave.",
            city = "Zamboanga"
        )
                mockMvc.put("/employees/addresses/{id}/", address.id!!) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(body)
                }.andExpect {
                    status { isOk() }

                }
    }
}
