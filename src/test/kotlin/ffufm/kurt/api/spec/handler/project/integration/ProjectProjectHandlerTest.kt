package ffufm.kurt.api.spec.handler.project.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.kurt.api.PassTestBase
import ffufm.kurt.api.repositories.project.ProjectProjectRepository
import ffufm.kurt.api.spec.dbo.project.ProjectProject
import ffufm.kurt.api.spec.handler.utils.EntityGenerator
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class ProjectProjectHandlerTest : PassTestBase() {

    @Test
    @WithMockUser
    fun `test createProject should return isOk`() {
        val employee = employeeEmployeeRepository.save(
            EntityGenerator.createEmployee())
        val body = EntityGenerator.createProject()
        mockMvc.post("/projects/{employeeId}/", employee.id) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.asyncDispatch().andExpect {
            status { isOk() }
        }
    }


    @Test
    @WithMockUser
    fun `test getAllProject should return isOk`() {
        val employee = employeeEmployeeRepository.save(
            EntityGenerator.createEmployee())
        val body = projectProjectRepository.save(
            EntityGenerator.createProject().copy(employee = employee))
        val maxResults = 100
        val page = 0

        projectProjectRepository.saveAll(
            listOf(
                body,
                body.copy(
                    status = "ON-GOING"
                )
            )
        )
        mockMvc.get("/projects/?page=$page&maxResult=$maxResults") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.asyncDispatch().andExpect {
            status { isOk() }
        }
    }


    @Test
    @WithMockUser
    fun `test removeProject should return isOk`() {
        val employee = employeeEmployeeRepository.save(
            EntityGenerator.createEmployee())
        val project = projectProjectRepository.save(
            EntityGenerator.createProject().copy(
                employee = employee
            )
        )
        mockMvc.delete("/projects/{id}/", project.id) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.asyncDispatch().andExpect {
            status { isOk() }
        }
    }

    @Test
    @WithMockUser
    fun `test updateProject should return isOk`() {
        val employee = employeeEmployeeRepository.save(
            EntityGenerator.createEmployee())
        val project = projectProjectRepository.save(
            EntityGenerator.createProject().copy(
                employee = employee
            )
        )
        val body = project.copy(
            status = "COMPLETED"
        )
        mockMvc.put("/projects/{id}/", project.id) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.asyncDispatch().andExpect {
            status { isOk() }
        }
    }
}
