package ffufm.kurt.api.spec.handler.employee.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.kurt.api.PassTestBase
import ffufm.kurt.api.repositories.employee.EmployeeContactRepository
import ffufm.kurt.api.spec.dbo.employee.EmployeeContact
import ffufm.kurt.api.spec.handler.utils.EntityGenerator
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class EmployeeContactHandlerTest : PassTestBase() {

    @Test
    @WithMockUser
    fun `test createContacts should return isOk`() {
        val employee = employeeEmployeeRepository.save(EntityGenerator.createEmployee())
        val body = EntityGenerator.createContacts().copy(
            employee = employee
        )
        mockMvc.post("/employees/{id}/contacts/", employee.id!!) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.asyncDispatch().andExpect {
            status { isOk() }
        }
    }

    @Test
    @WithMockUser
    fun `test deleteContacts should return isOk`() {
        val employee = employeeEmployeeRepository.save(EntityGenerator.createEmployee())
        val contactDetail = employeeContactRepository.save(EntityGenerator.createContacts().copy(
            employee = employee
        ))
        mockMvc.delete("/employees/contacts/{id}/", contactDetail.id!!) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.asyncDispatch().andExpect {
            status { isOk() }
        }
    }

    @Test
    @WithMockUser
    fun `test updateContacts should return isOk`() {
        val employee = employeeEmployeeRepository.save(EntityGenerator.createEmployee())
        val contactDetail = employeeContactRepository.save(
            EntityGenerator.createContacts().copy(
                employee = employee
            ))

        val body = contactDetail.copy(
            contactDetail = "09182575936",
            contactType = "Phone Number",
        )
        mockMvc.put("/employees/{id}/", contactDetail.id!!) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.asyncDispatch().andExpect {
            status { isOk() }
        }
    }
}
