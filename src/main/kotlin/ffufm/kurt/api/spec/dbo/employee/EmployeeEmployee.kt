package ffufm.kurt.api.spec.dbo.employee

import am.ik.yavi.builder.ValidatorBuilder
import de.ffuf.pass.common.models.PassDTO
import de.ffuf.pass.common.models.PassDTOModel
import de.ffuf.pass.common.models.PassDtoSerializer
import de.ffuf.pass.common.models.PassModelValidation
import de.ffuf.pass.common.utilities.extensions.toEntities
import de.ffuf.pass.common.utilities.extensions.toSafeDtos
import ffufm.kurt.api.spec.dbo.leaverecord.LeaverecordLeaveRecord
import ffufm.kurt.api.spec.dbo.leaverecord.LeaverecordLeaveRecordDTO
import ffufm.kurt.api.spec.dbo.project.ProjectProject
import ffufm.kurt.api.spec.dbo.project.ProjectProjectDTO
import ffufm.kurt.api.spec.dbo.task.TaskTask
import ffufm.kurt.api.spec.dbo.task.TaskTaskDTO
import ffufm.kurt.api.utils.UserTypeEnum
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Lob
import javax.persistence.OneToMany
import javax.persistence.Table
import kotlin.Long
import kotlin.String
import kotlin.collections.List
import kotlin.reflect.KClass
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

@Entity(name = "EmployeeEmployee")
@Table(name = "employee_employee")
data class EmployeeEmployee(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    /**
     * first name of the employee
     * Sample: Greg
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "first_name"
    )
    @Lob
    val firstName: String = "",
    /**
     * last name of the employee
     * Sample: Nue
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "last_name"
    )
    @Lob
    val lastName: String = "",
    /**
     * email of the employee
     * Sample: greg.neu@f-bootcamp.com
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "email"
    )
    @Lob
    val email: String = "",
    /**
     * position of the employee
     * Sample: Monteur
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "position"
    )
    @Lob
    val position: String = "",
    /**
     * Employee or superior
     * Sample: Employee
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "userType"
    )
    @Lob
    val userType:String,
    @OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
    val addresses: List<EmployeeAddress>? = mutableListOf(),
    @OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
    val contacts: List<EmployeeContact>? = mutableListOf(),
    @OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
    val leaveRecords: List<LeaverecordLeaveRecord>? = mutableListOf(),
    @OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
    val projects: List<ProjectProject>? = mutableListOf(),
    @OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
    val tasks: List<TaskTask>? = mutableListOf()
) : PassDTOModel<EmployeeEmployee, EmployeeEmployeeDTO, Long>() {
    override fun toDto(): EmployeeEmployeeDTO =
            super.toDtoInternal(EmployeeEmployeeSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<EmployeeEmployee, EmployeeEmployeeDTO, Long>,
            EmployeeEmployeeDTO, Long>>)

    override fun readId(): Long? = this.id

    override fun toString(): String = super.toString()
}

data class EmployeeEmployeeDTO(
    val id: Long? = null,
    /**
     * first name of the employee
     * Sample: Greg
     */
    val firstName: String? = "",
    /**
     * last name of the employee
     * Sample: Nue
     */
    val lastName: String? = "",
    /**
     * email of the employee
     * Sample: greg.neu@f-bootcamp.com
     */
    val email: String? = "",
    /**
     * position of the employee
     * Sample: Monteur
     */
    val position: String? = "",
    /**
     * Employee or superior
     * Sample: Employee
     */
    val userType: String? = "",
    val addresses: List<EmployeeAddressDTO>? = null,
    val contacts: List<EmployeeContactDTO>? = null,
    val leaveRecords: List<LeaverecordLeaveRecordDTO>? = null,
    val projects: List<ProjectProjectDTO>? = null,
    val tasks: List<TaskTaskDTO>? = null
) : PassDTO<EmployeeEmployee, Long>() {
    override fun toEntity(): EmployeeEmployee =
            super.toEntityInternal(EmployeeEmployeeSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<EmployeeEmployee, PassDTO<EmployeeEmployee, Long>,
            Long>, PassDTO<EmployeeEmployee, Long>, Long>>)

    override fun readId(): Long? = this.id
}

@Component
class EmployeeEmployeeSerializer : PassDtoSerializer<EmployeeEmployee, EmployeeEmployeeDTO, Long>()
        {
    override fun toDto(entity: EmployeeEmployee): EmployeeEmployeeDTO = cycle(entity) {
        EmployeeEmployeeDTO(
                id = entity.id,
        firstName = entity.firstName,
        lastName = entity.lastName,
        email = entity.email,
        position = entity.position,
        userType = entity.userType,
        addresses = entity.addresses?.toSafeDtos(),
        contacts = entity.contacts?.toSafeDtos(),
        leaveRecords = entity.leaveRecords?.toSafeDtos(),
        projects = entity.projects?.toSafeDtos(),
        tasks = entity.tasks?.toSafeDtos()
                )}

    override fun toEntity(dto: EmployeeEmployeeDTO): EmployeeEmployee = EmployeeEmployee(
            id = dto.id,
    firstName = dto.firstName ?: "",
    lastName = dto.lastName ?: "",
    email = dto.email ?: "",
    position = dto.position ?: "",
    userType = dto.userType ?: "",
    addresses = dto.addresses?.toEntities() ?: emptyList(),
    contacts = dto.contacts?.toEntities() ?: emptyList(),
    leaveRecords = dto.leaveRecords?.toEntities() ?: emptyList(),
    projects = dto.projects?.toEntities() ?: emptyList(),
    tasks = dto.tasks?.toEntities() ?: emptyList()
            )
    override fun idDto(id: Long): EmployeeEmployeeDTO = EmployeeEmployeeDTO(
            id = id,
    firstName = null,
    lastName = null,
    email = null,
    position = null,
    userType = null,

            )}

@Service("employee.EmployeeEmployeeValidator")
class EmployeeEmployeeValidator : PassModelValidation<EmployeeEmployee> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<EmployeeEmployee>):
            ValidatorBuilder<EmployeeEmployee> = validatorBuilder.apply {
    }
}

@Service("employee.EmployeeEmployeeDTOValidator")
class EmployeeEmployeeDTOValidator : PassModelValidation<EmployeeEmployeeDTO> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<EmployeeEmployeeDTO>):
            ValidatorBuilder<EmployeeEmployeeDTO> = validatorBuilder.apply {
    }
}
