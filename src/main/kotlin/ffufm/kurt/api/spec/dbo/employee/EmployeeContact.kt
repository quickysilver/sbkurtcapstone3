package ffufm.kurt.api.spec.dbo.employee

import am.ik.yavi.builder.ValidatorBuilder
import am.ik.yavi.builder.konstraint
import am.ik.yavi.builder.konstraintOnObject
import de.ffuf.pass.common.models.PassDTO
import de.ffuf.pass.common.models.PassDTOModel
import de.ffuf.pass.common.models.PassDtoSerializer
import de.ffuf.pass.common.models.PassModelValidation
import de.ffuf.pass.common.models.idDto
import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.utilities.extensions.konstraint
import de.ffuf.pass.common.utilities.extensions.toEntities
import de.ffuf.pass.common.utilities.extensions.toSafeDtos
import ffufm.kurt.api.spec.dbo.employee.EmployeeEmployeeSerializer
import java.util.TreeSet
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Index
import javax.persistence.JoinColumn
import javax.persistence.Lob
import javax.persistence.ManyToOne
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import kotlin.Boolean
import kotlin.Long
import kotlin.String
import kotlin.reflect.KClass
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.ColumnDefault
import org.hibernate.annotations.FetchMode
import org.springframework.beans.factory.getBeansOfType
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

/**
 * Contacts of the employee
 */
@Entity(name = "EmployeeContact")
@Table(name = "employee_contact")
data class EmployeeContact(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    /**
     * contact detail of the contacts
     * Sample: +49 1234 56 789 01
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "contact_detail"
    )
    @Lob
    val contactDetail: String = "",
    /**
     * contact type of the contacts
     * Sample: Telephone Number
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "contact_type"
    )
    @Lob
    val contactType: String = "",
    /**
     * If the contacts is primary
     * Sample: TRUE
     */
    @ColumnDefault("TRUE")
    @Column(name = "is_primary")
    val isPrimary: Boolean = false,
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(nullable = true)
    val employee: EmployeeEmployee? = null
) : PassDTOModel<EmployeeContact, EmployeeContactDTO, Long>() {
    override fun toDto(): EmployeeContactDTO = super.toDtoInternal(EmployeeContactSerializer::class
            as KClass<PassDtoSerializer<PassDTOModel<EmployeeContact, EmployeeContactDTO, Long>,
            EmployeeContactDTO, Long>>)

    override fun readId(): Long? = this.id

    override fun toString(): String = super.toString()
}

/**
 * Contacts of the employee
 */
data class EmployeeContactDTO(
    val id: Long? = null,
    /**
     * contact detail of the contacts
     * Sample: +49 1234 56 789 01
     */
    val contactDetail: String? = "",
    /**
     * contact type of the contacts
     * Sample: Telephone Number
     */
    val contactType: String? = "",
    /**
     * If the contacts is primary
     * Sample: TRUE
     */
    val isPrimary: Boolean? = false,
    val employee: EmployeeEmployeeDTO? = null
) : PassDTO<EmployeeContact, Long>() {
    override fun toEntity(): EmployeeContact =
            super.toEntityInternal(EmployeeContactSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<EmployeeContact, PassDTO<EmployeeContact, Long>,
            Long>, PassDTO<EmployeeContact, Long>, Long>>)

    override fun readId(): Long? = this.id
}

@Component
class EmployeeContactSerializer : PassDtoSerializer<EmployeeContact, EmployeeContactDTO, Long>() {
    override fun toDto(entity: EmployeeContact): EmployeeContactDTO = cycle(entity) {
        EmployeeContactDTO(
                id = entity.id,
        contactDetail = entity.contactDetail,
        contactType = entity.contactType,
        isPrimary = entity.isPrimary,
        employee = entity.employee?.idDto() ?: entity.employee?.toDto()
                )}

    override fun toEntity(dto: EmployeeContactDTO): EmployeeContact = EmployeeContact(
            id = dto.id,
    contactDetail = dto.contactDetail ?: "",
    contactType = dto.contactType ?: "",
    isPrimary = dto.isPrimary ?: false,
    employee = dto.employee?.toEntity()
            )
    override fun idDto(id: Long): EmployeeContactDTO = EmployeeContactDTO(
            id = id,
    contactDetail = null,
    contactType = null,
    isPrimary = null,

            )}

@Service("employee.EmployeeContactValidator")
class EmployeeContactValidator : PassModelValidation<EmployeeContact> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<EmployeeContact>):
            ValidatorBuilder<EmployeeContact> = validatorBuilder.apply {
    }
}

@Service("employee.EmployeeContactDTOValidator")
class EmployeeContactDTOValidator : PassModelValidation<EmployeeContactDTO> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<EmployeeContactDTO>):
            ValidatorBuilder<EmployeeContactDTO> = validatorBuilder.apply {
    }
}
