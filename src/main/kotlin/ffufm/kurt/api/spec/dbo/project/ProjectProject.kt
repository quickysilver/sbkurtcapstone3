package ffufm.kurt.api.spec.dbo.project

import am.ik.yavi.builder.ValidatorBuilder
import am.ik.yavi.builder.konstraint
import am.ik.yavi.builder.konstraintOnObject
import de.ffuf.pass.common.models.PassDTO
import de.ffuf.pass.common.models.PassDTOModel
import de.ffuf.pass.common.models.PassDtoSerializer
import de.ffuf.pass.common.models.PassModelValidation
import de.ffuf.pass.common.models.idDto
import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.utilities.extensions.konstraint
import de.ffuf.pass.common.utilities.extensions.toEntities
import de.ffuf.pass.common.utilities.extensions.toSafeDtos
import ffufm.kurt.api.spec.dbo.employee.EmployeeEmployee
import ffufm.kurt.api.spec.dbo.employee.EmployeeEmployeeDTO
import ffufm.kurt.api.spec.dbo.employee.EmployeeEmployeeSerializer
import ffufm.kurt.api.spec.dbo.task.TaskTask
import ffufm.kurt.api.spec.dbo.task.TaskTaskDTO
import ffufm.kurt.api.spec.dbo.task.TaskTaskSerializer
import java.util.TreeSet
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Index
import javax.persistence.JoinColumn
import javax.persistence.Lob
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import kotlin.Long
import kotlin.String
import kotlin.collections.List
import kotlin.reflect.KClass
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.FetchMode
import org.springframework.beans.factory.getBeansOfType
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

/**
 * For the projects
 */
@Entity(name = "ProjectProject")
@Table(name = "project_project")
data class ProjectProject(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    /**
     * name of the project
     * Sample: Time Tracker
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "name"
    )
    @Lob
    val name: String = "",
    /**
     * the description of the project
     * Sample: For adding new time tracker
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "description"
    )
    @Lob
    val description: String = "",
    /**
     * status of the project
     * Sample: ON-GOING
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "status"
    )
    @Lob
    val status: String = "",
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(nullable = true)
    val employee: EmployeeEmployee? = null,
    @OneToMany(mappedBy = "project", fetch = FetchType.LAZY)
    val tasks: List<TaskTask>? = mutableListOf()
) : PassDTOModel<ProjectProject, ProjectProjectDTO, Long>() {
    override fun toDto(): ProjectProjectDTO = super.toDtoInternal(ProjectProjectSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<ProjectProject, ProjectProjectDTO, Long>,
            ProjectProjectDTO, Long>>)

    override fun readId(): Long? = this.id

    override fun toString(): String = super.toString()
}

/**
 * For the projects
 */
data class ProjectProjectDTO(
    val id: Long? = null,
    /**
     * name of the project
     * Sample: Time Tracker
     */
    val name: String? = "",
    /**
     * the description of the project
     * Sample: For adding new time tracker
     */
    val description: String? = "",
    /**
     * status of the project
     * Sample: ON-GOING
     */
    val status: String? = "",
    val employee: EmployeeEmployeeDTO? = null,
    val tasks: List<TaskTaskDTO>? = null
) : PassDTO<ProjectProject, Long>() {
    override fun toEntity(): ProjectProject = super.toEntityInternal(ProjectProjectSerializer::class
            as KClass<PassDtoSerializer<PassDTOModel<ProjectProject, PassDTO<ProjectProject, Long>,
            Long>, PassDTO<ProjectProject, Long>, Long>>)

    override fun readId(): Long? = this.id
}

@Component
class ProjectProjectSerializer : PassDtoSerializer<ProjectProject, ProjectProjectDTO, Long>() {
    override fun toDto(entity: ProjectProject): ProjectProjectDTO = cycle(entity) {
        ProjectProjectDTO(
                id = entity.id,
        name = entity.name,
        description = entity.description,
        status = entity.status,
        employee = entity.employee?.idDto() ?: entity.employee?.toDto(),
        tasks = entity.tasks?.toSafeDtos()
                )}

    override fun toEntity(dto: ProjectProjectDTO): ProjectProject = ProjectProject(
            id = dto.id,
    name = dto.name ?: "",
    description = dto.description ?: "",
    status = dto.status ?: "",
    employee = dto.employee?.toEntity(),
    tasks = dto.tasks?.toEntities() ?: emptyList()
            )
    override fun idDto(id: Long): ProjectProjectDTO = ProjectProjectDTO(
            id = id,
    name = null,
    description = null,
    status = null,

            )}

@Service("project.ProjectProjectValidator")
class ProjectProjectValidator : PassModelValidation<ProjectProject> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<ProjectProject>):
            ValidatorBuilder<ProjectProject> = validatorBuilder.apply {
    }
}

@Service("project.ProjectProjectDTOValidator")
class ProjectProjectDTOValidator : PassModelValidation<ProjectProjectDTO> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<ProjectProjectDTO>):
            ValidatorBuilder<ProjectProjectDTO> = validatorBuilder.apply {
    }
}
