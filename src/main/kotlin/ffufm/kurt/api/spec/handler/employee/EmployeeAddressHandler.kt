package ffufm.kurt.api.spec.handler.employee

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.kurt.api.spec.dbo.employee.EmployeeAddressDTO
import kotlin.Long
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface EmployeeAddressDatabaseHandler {
    /**
     * Create Address: Creates a new Address object
     * HTTP Code 200: The created Address
     */
    suspend fun create(body: EmployeeAddressDTO, id: Long): EmployeeAddressDTO

    /**
     * Delete Address by id.: Deletes one specific Address.
     * HTTP Code 200: Sucessfully deleted address
     */
    suspend fun remove(id: Long): EmployeeAddressDTO

    /**
     * Update the Address: Updates an existing Address
     * HTTP Code 200: The updated model
     */
    suspend fun update(body: EmployeeAddressDTO, id: Long): EmployeeAddressDTO
}

@Controller("employee.Address")
class EmployeeAddressHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: EmployeeAddressDatabaseHandler

    /**
     * Create Address: Creates a new Address object
     * HTTP Code 200: The created Address
     */
    @RequestMapping(value = ["/employees/{id:\\d+}/addresses/"], method = [RequestMethod.POST])
    suspend fun create(@RequestBody body: EmployeeAddressDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.create(body, id) }
    }

    /**
     * Delete Address by id.: Deletes one specific Address.
     * HTTP Code 200: Sucessfully deleted address
     */
    @RequestMapping(value = ["/employees/addresses/{id:\\d+}/"], method = [RequestMethod.DELETE])
    suspend fun remove(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.remove(id) }
    }

    /**
     * Update the Address: Updates an existing Address
     * HTTP Code 200: The updated model
     */
    @RequestMapping(value = ["/employees/addresses/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun update(@RequestBody body: EmployeeAddressDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.update(body, id) }
    }
}
