package ffufm.kurt.api.spec.handler.employee

import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.kurt.api.spec.dbo.employee.EmployeeContactDTO
import kotlin.Long
import kotlin.String
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

interface EmployeeContactDatabaseHandler {
    /**
     * Create Contact: Creates a new Contact object
     * HTTP Code 201: The created Contact
     */
    suspend fun createContacts(body: EmployeeContactDTO, id: Long): EmployeeContactDTO

    /**
     * Delete Contact by id.: Deletes one specific Contact.
     * HTTP Code 200: Successfully deleted contacts
     */
    suspend fun deleteContacts(id: Long): EmployeeContactDTO

    /**
     * Update the Contact: Updates an existing Contact
     * HTTP Code 200: The updated model
     */
    suspend fun updateContacts(body: EmployeeContactDTO, id: Long): EmployeeContactDTO
}

@Controller("employee.Contact")
class EmployeeContactHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: EmployeeContactDatabaseHandler

    /**
     * Create Contact: Creates a new Contact object
     * HTTP Code 201: The created Contact
     */
    @RequestMapping(value = ["/employees/{id:.*}/contacts/"], method = [RequestMethod.POST])
    suspend fun createContacts(@RequestBody body: EmployeeContactDTO, @PathVariable("id")
            id: Long): ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.createContacts(body, id) }
    }

    /**
     * Delete Contact by id.: Deletes one specific Contact.
     * HTTP Code 200: Successfully deleted contacts
     */
    @RequestMapping(value = ["/employees/contacts/{id:\\d+}/"], method = [RequestMethod.DELETE])
    suspend fun deleteContacts(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.deleteContacts(id) }
    }

    /**
     * Update the Contact: Updates an existing Contact
     * HTTP Code 200: The updated model
     */
    @RequestMapping(value = ["/employees/contacts/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun updateContacts(@RequestBody body: EmployeeContactDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.updateContacts(body, id) }
    }
}
