package ffufm.kurt.api.spec.handler.project

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.kurt.api.spec.dbo.project.ProjectProjectDTO
import kotlin.Int
import kotlin.Long
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface ProjectProjectDatabaseHandler {
    /**
     * Create Project: Creates a new Project object
     * HTTP Code 201: The created Project
     */
    suspend fun create(body: ProjectProjectDTO, employeeId: Long): ProjectProjectDTO

    /**
     * Get all Projects: Returns all Projects from the system that the user has access to.
     * HTTP Code 200: List of Projects
     */
    suspend fun getAll(maxResults: Int = 100, page: Int = 0): Page<ProjectProjectDTO>

    /**
     * Delete Project by id.: Deletes one specific Project.
     * HTTP Code 200: Successfully deleted project
     */
    suspend fun remove(id: Long)

    /**
     * Update the Project: Updates an existing Project
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    suspend fun update(body: ProjectProjectDTO, id: Long): ProjectProjectDTO
}

@Controller("project.Project")
class ProjectProjectHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: ProjectProjectDatabaseHandler

    /**
     * Create Project: Creates a new Project object
     * HTTP Code 201: The created Project
     */
    @RequestMapping(value = ["/projects/"], method = [RequestMethod.POST])
    suspend fun create(@RequestBody body: ProjectProjectDTO, @PathVariable("employeeId") employeeId: Long): ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.create(body, employeeId) }
    }

    /**
     * Get all Projects: Returns all Projects from the system that the user has access to.
     * HTTP Code 200: List of Projects
     */
    @RequestMapping(value = ["/projects/"], method = [RequestMethod.GET])
    suspend fun getAll(@RequestParam("maxResults") maxResults: Int? = 100, @RequestParam("page")
            page: Int? = 0): ResponseEntity<*> {

        return paging { databaseHandler.getAll(maxResults ?: 100, page ?: 0) }
    }

    /**
     * Delete Project by id.: Deletes one specific Project.
     * HTTP Code 200: Successfully deleted project
     */
    @RequestMapping(value = ["/projects/{id:\\d+}/"], method = [RequestMethod.DELETE])
    suspend fun remove(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.remove(id) }
    }

    /**
     * Update the Project: Updates an existing Project
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    @RequestMapping(value = ["/projects/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun update(@RequestBody body: ProjectProjectDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.update(body, id) }
    }
}
