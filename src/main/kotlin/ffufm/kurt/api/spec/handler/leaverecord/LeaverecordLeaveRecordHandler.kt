package ffufm.kurt.api.spec.handler.leaverecord

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.kurt.api.spec.dbo.leaverecord.LeaverecordLeaveRecordDTO
import kotlin.Int
import kotlin.Long
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface LeaverecordLeaveRecordDatabaseHandler {
    /**
     * Create Leave Record: Create new leave record
     * HTTP Code 200: To create leave record
     */
    suspend fun createleaveRecords(body: LeaverecordLeaveRecordDTO, id: Long):
            LeaverecordLeaveRecordDTO

    /**
     * Find by Employee: Finds LeaveRecords by the parent Employee id
     * HTTP Code 200: List of LeaveRecords items
     */
    suspend fun getByEmployee(
        id: Long,
        maxResults: Int = 100,
        page: Int = 0
    ): Page<LeaverecordLeaveRecordDTO>

    /**
     * Finds LeaveRecords by ID: Returns LeaveRecords based on ID
     * HTTP Code 200: The LeaveRecord object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    suspend fun getById(id: Long): LeaverecordLeaveRecordDTO?

    /**
     * Update the LeaveRecord: Updates an existing LeaveRecord
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    suspend fun update(body: LeaverecordLeaveRecordDTO, id: Long): LeaverecordLeaveRecordDTO
}

@Controller("leaverecord.LeaveRecord")
class LeaverecordLeaveRecordHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: LeaverecordLeaveRecordDatabaseHandler

    /**
     * Create Leave Record: Create new leave record
     * HTTP Code 200: To create leave record
     */
    @RequestMapping(value = ["/employees/{id:\\d+}/leave-records/"], method = [RequestMethod.POST])
    suspend fun createleaveRecords(@RequestBody body: LeaverecordLeaveRecordDTO, @PathVariable("id")
            id: Long): ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.createleaveRecords(body, id) }
    }

    /**
     * Find by Employee: Finds LeaveRecords by the parent Employee id
     * HTTP Code 200: List of LeaveRecords items
     */
    @RequestMapping(value = ["/employees/{id:\\d+}/leaverecords/"], method = [RequestMethod.GET])
    suspend fun getByEmployee(
        @PathVariable("id") id: Long,
        @RequestParam("maxResults") maxResults: Int? = 100,
        @RequestParam("page") page: Int? = 0
    ): ResponseEntity<*> {

        return paging { databaseHandler.getByEmployee(id, maxResults ?: 100, page ?: 0) }
    }

    /**
     * Finds LeaveRecords by ID: Returns LeaveRecords based on ID
     * HTTP Code 200: The LeaveRecord object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    @RequestMapping(value = ["/leaverecords/{id:\\d+}/"], method = [RequestMethod.GET])
    suspend fun getById(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.getById(id) ?: throw
                ResponseStatusException(HttpStatus.NOT_FOUND) }
    }

    /**
     * Update the LeaveRecord: Updates an existing LeaveRecord
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    @RequestMapping(value = ["/leaverecords/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun update(@RequestBody body: LeaverecordLeaveRecordDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.update(body, id) }
    }
}
