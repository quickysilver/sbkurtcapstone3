package ffufm.kurt.api.spec.handler.task

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.kurt.api.spec.dbo.task.TaskTaskDTO
import kotlin.Any
import kotlin.Long
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface TaskTaskDatabaseHandler {
    /**
     * : 
     */
    suspend fun createTasks(
        body: TaskTaskDTO,
        employeeId: Long,
        projectId: Long
    ): ResponseEntity<Any>

    /**
     * Delete Task by id.: Deletes one specific Task.
     * HTTP Code 200: Successfully deleted task
     */
    suspend fun remove(id: Long): TaskTaskDTO

    /**
     * Update the Task: Updates an existing Task
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    suspend fun update(body: TaskTaskDTO, id: Long): TaskTaskDTO
}

@Controller("task.Task")
class TaskTaskHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: TaskTaskDatabaseHandler

    /**
     * : 
     */
    @RequestMapping(value = ["/employees/{employeeId:\\d+}/projects/{projectId:\\d+}/tasks/"],
            method = [RequestMethod.POST])
    suspend fun createTasks(
        @RequestBody body: TaskTaskDTO,
        @PathVariable("employeeId") employeeId: Long,
        @PathVariable("projectId") projectId: Long
    ): ResponseEntity<*> {
        body.validateOrThrow()
        return serverResponse { databaseHandler.createTasks(body, employeeId, projectId) }
    }

    /**
     * Delete Task by id.: Deletes one specific Task.
     * HTTP Code 200: Successfully deleted task
     */
    @RequestMapping(value = ["/tasks/{id:\\d+}/"], method = [RequestMethod.DELETE])
    suspend fun remove(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.remove(id) }
    }

    /**
     * Update the Task: Updates an existing Task
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    @RequestMapping(value = ["/tasks/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun update(@RequestBody body: TaskTaskDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.update(body, id) }
    }
}
