package ffufm.kurt.api.utils

enum class UserTypeEnum(val value: String) {
    Employee("Employee"),
    Superior("Superior")
}