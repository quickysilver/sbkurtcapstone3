package ffufm.kurt.api.repositories.employee

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.kurt.api.spec.dbo.employee.EmployeeContact
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Repository
interface EmployeeContactRepository : PassRepository<EmployeeContact, Long> {
    @Query(
        "SELECT t from EmployeeContact t LEFT JOIN FETCH t.employee",
        countQuery = "SELECT count(id) FROM EmployeeContact"
    )
    fun findAllAndFetchEmployee(pageable: Pageable): Page<EmployeeContact>

    @Modifying
    @Transactional
    @Query(
        """
            UPDATE EmployeeContact t SET t.isPrimary = false
            WHERE t.employee.id = :employeeId
        """
    )

    fun updateIsPrimaryToFalse(employeeId: Long): Unit
    @Query(
        """
            SELECT COUNT(t) FROM EmployeeContact t WHERE t.employee.id = :employeeId
        """
    )
    fun countContactsByUserId(employeeId: Long): Int
}
