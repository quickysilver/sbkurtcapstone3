package ffufm.kurt.api.repositories.employee

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.kurt.api.spec.dbo.employee.EmployeeEmployee
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface EmployeeEmployeeRepository : PassRepository<EmployeeEmployee, Long> {
    @Query(
        "SELECT t from EmployeeEmployee t LEFT JOIN FETCH t.addresses",
        countQuery = "SELECT count(id) FROM EmployeeEmployee"
    )
    fun findAllAndFetchAddresses(pageable: Pageable): Page<EmployeeEmployee>

    @Query(
        "SELECT t from EmployeeEmployee t LEFT JOIN FETCH t.contacts",
        countQuery = "SELECT count(id) FROM EmployeeEmployee"
    )
    fun findAllAndFetchContacts(pageable: Pageable): Page<EmployeeEmployee>

    @Query(
        "SELECT t from EmployeeEmployee t LEFT JOIN FETCH t.leaveRecords",
        countQuery = "SELECT count(id) FROM EmployeeEmployee"
    )
    fun findAllAndFetchLeaveRecords(pageable: Pageable): Page<EmployeeEmployee>

    @Query(
        "SELECT t from EmployeeEmployee t LEFT JOIN FETCH t.projects",
        countQuery = "SELECT count(id) FROM EmployeeEmployee"
    )
    fun findAllAndFetchProjects(pageable: Pageable): Page<EmployeeEmployee>

    @Query(
        "SELECT t from EmployeeEmployee t LEFT JOIN FETCH t.tasks",
        countQuery = "SELECT count(id) FROM EmployeeEmployee"
    )
    fun findAllAndFetchTasks(pageable: Pageable): Page<EmployeeEmployee>
    @Query(
        """
            SELECT CASE WHEN COUNT(t) > 0 THEN TRUE ELSE FALSE END
            FROM EmployeeEmployee t WHERE t.email = :email
        """
    )
    fun doesEmailExist(email: String): Boolean
}
