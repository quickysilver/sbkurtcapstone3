package ffufm.kurt.api.repositories.employee

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.kurt.api.spec.dbo.employee.EmployeeAddress
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface EmployeeAddressRepository : PassRepository<EmployeeAddress, Long> {
    @Query(
        "SELECT t from EmployeeAddress t LEFT JOIN FETCH t.employee",
        countQuery = "SELECT count(id) FROM EmployeeAddress"
    )
    fun findAllAndFetchEmployee(pageable: Pageable): Page<EmployeeAddress>
    @Query(
        """
            SELECT CASE WHEN COUNT(t) > 0 THEN TRUE ELSE FALSE END
            FROM EmployeeAddress t WHERE 
            t.street = :street AND
            t.city = :city 
        """
    )
    fun doesAddressExist(street: String, city: String): Boolean
}
