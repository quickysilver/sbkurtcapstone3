package ffufm.kurt.api.handlerimpl.project

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import de.ffuf.pass.common.utilities.extensions.toDtos
import ffufm.kurt.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.kurt.api.repositories.project.ProjectProjectRepository
import ffufm.kurt.api.spec.dbo.project.ProjectProject
import ffufm.kurt.api.spec.dbo.project.ProjectProjectDTO
import ffufm.kurt.api.spec.handler.project.ProjectProjectDatabaseHandler
import kotlin.Int
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException

@Component("project.ProjectProjectHandler")
class ProjectProjectHandlerImpl(
    private val employeeEmployeeRepository: EmployeeEmployeeRepository
) : PassDatabaseHandler<ProjectProject, ProjectProjectRepository>(),
        ProjectProjectDatabaseHandler {
        /**
     * Create Project: Creates a new Project object
     * HTTP Code 201: The created Project
     */
    override suspend fun create(body: ProjectProjectDTO, employeeId: Long): ProjectProjectDTO {
            val bodyEntity = body.toEntity()
            val employee = employeeEmployeeRepository.findById(employeeId).orElseThrow404(employeeId)

            return repository.save(bodyEntity.copy(
              employee = employee
            )).toDto()
    }

    /**
     * Get all Projects: Returns all Projects from the system that the user has access to.
     * HTTP Code 200: List of Projects
     */
    override suspend fun getAll(maxResults: Int, page: Int): Page<ProjectProjectDTO> {
        TODO("not checked yet")
        return repository.findAll(Pageable.unpaged()).toDtos()
    }

    /**
     * Delete Project by id.: Deletes one specific Project.
     * HTTP Code 200: Successfully deleted project
     */
    override suspend fun remove(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.delete(original)
    }

    /**
     * Update the Project: Updates an existing Project
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    override suspend fun update(body: ProjectProjectDTO, id: Long): ProjectProjectDTO {
        val original = repository.findById(id).orElseThrow{
            ResponseStatusException(
                HttpStatus.NOT_FOUND, "Project $id does not exists"
            )
        }

        return repository.save(original.copy(
            name = body.name!!,
            description = body.description!!,
            status = body.status!!
        )).toDto()
    }
}
