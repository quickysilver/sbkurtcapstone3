package ffufm.kurt.api.handlerimpl.employee

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffufm.kurt.api.repositories.employee.EmployeeContactRepository
import ffufm.kurt.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.kurt.api.spec.dbo.employee.EmployeeContact
import ffufm.kurt.api.spec.dbo.employee.EmployeeContactDTO
import ffufm.kurt.api.spec.handler.employee.EmployeeContactDatabaseHandler
import ffufm.kurt.api.utils.Constants
import kotlin.Long
import kotlin.String
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException

@Component("employee.EmployeeContactHandler")
class EmployeeContactHandlerImpl(
    private val employeeRepository: EmployeeEmployeeRepository) :
    PassDatabaseHandler<EmployeeContact, EmployeeContactRepository>(), EmployeeContactDatabaseHandler {
    /**
     * Create Contact: Creates a new Contact object
     * HTTP Code 201: The created Contact
     */
    override suspend fun createContacts(body: EmployeeContactDTO, id: Long): EmployeeContactDTO {
        val bodyEntity = body.toEntity()
        val employee = employeeRepository.findById(id).orElseThrow404(id)
        if (bodyEntity.isPrimary) {
            repository.updateIsPrimaryToFalse(id)
        }
        val contactDetailsCount = repository.countContactsByUserId(id)
        if (contactDetailsCount >= Constants.MAX_CONTACTS) {
            throw ResponseStatusException (HttpStatus.BAD_REQUEST, "User cannot create more than four contacts.")
        }
        return repository.save(bodyEntity.copy(
            employee = employee
        )).toDto()
    }

    /**
     * Delete Contact by id.: Deletes one specific Contact.
     * HTTP Code 200: Successfully deleted contacts
     */
    override suspend fun deleteContacts(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.delete(original)
    }

    /**
     * Update the Contact: Updates an existing Contact
     * HTTP Code 200: The updated model
     */
    override suspend fun updateContacts(body: EmployeeContactDTO, id: Long): EmployeeContactDTO {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.save(original).toDto()
    }
}
