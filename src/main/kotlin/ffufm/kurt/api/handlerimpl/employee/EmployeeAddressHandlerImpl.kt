package ffufm.kurt.api.handlerimpl.employee

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffufm.kurt.api.repositories.employee.EmployeeAddressRepository
import ffufm.kurt.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.kurt.api.spec.dbo.employee.EmployeeAddress
import ffufm.kurt.api.spec.dbo.employee.EmployeeAddressDTO
import ffufm.kurt.api.spec.handler.employee.EmployeeAddressDatabaseHandler
import kotlin.Long
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException

@Component("employee.EmployeeAddressHandler")
class EmployeeAddressHandlerImpl(
    private val employeeRepository: EmployeeEmployeeRepository) :
    PassDatabaseHandler<EmployeeAddress, EmployeeAddressRepository>(), EmployeeAddressDatabaseHandler {
    /**
     * Create Address: Creates a new Address object
     * HTTP Code 200: The created Address
     */
    override suspend fun create(body: EmployeeAddressDTO, id: Long): EmployeeAddressDTO {
        val bodyEntity = body.toEntity()
        val employee = employeeRepository.findById(id).orElseThrow404(id)
        if (repository.doesAddressExist(bodyEntity.street, bodyEntity.city)) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "Address ${body.street}  ${body.city} was already exists!")
        }
        return repository.save(bodyEntity.copy(
            employee = employee
        )).toDto()
    }

    /**
     * Delete Address by id.: Deletes one specific Address.
     * HTTP Code 200: Sucessfully deleted address
     */
    override suspend fun remove(id: Long){
        val original = repository.findById(id).orElseThrow404(id)
        return repository.delete(original)
    }

    /**
     * Update the Address: Updates an existing Address
     * HTTP Code 200: The updated model
     */
    override suspend fun update(body: EmployeeAddressDTO, id: Long): EmployeeAddressDTO {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.save(original).toDto()
    }
}
