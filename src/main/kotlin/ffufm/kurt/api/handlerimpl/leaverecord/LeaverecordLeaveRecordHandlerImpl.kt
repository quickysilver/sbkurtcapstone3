package ffufm.kurt.api.handlerimpl.leaverecord

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffufm.kurt.api.repositories.leaverecord.LeaverecordLeaveRecordRepository
import ffufm.kurt.api.spec.dbo.leaverecord.LeaverecordLeaveRecord
import ffufm.kurt.api.spec.dbo.leaverecord.LeaverecordLeaveRecordDTO
import ffufm.kurt.api.spec.handler.leaverecord.LeaverecordLeaveRecordDatabaseHandler
import kotlin.Int
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component("leaverecord.LeaverecordLeaveRecordHandler")
class LeaverecordLeaveRecordHandlerImpl : PassDatabaseHandler<LeaverecordLeaveRecord,
        LeaverecordLeaveRecordRepository>(), LeaverecordLeaveRecordDatabaseHandler {
    //    /**
//     * Create Leave Record: Create new leave record
//     * HTTP Code 200: To create leave record
//     */
//    override suspend fun createleaveRecords(body: LeaverecordLeaveRecord): LeaverecordLeaveRecord {
//        TODO("not checked yet")
//        return repository.save(body)
//    }
//
//    /**
//     * Find by Employee: Finds LeaveRecords by the parent Employee id
//     * HTTP Code 200: List of LeaveRecords items
//     */
//    override suspend fun getByEmployee(
//        id: Long,
//        maxResults: Int = 100,
//        page: Int = 0
//    ): Page<LeaverecordLeaveRecord> {
//        TODO("not checked yet")
//        return repository.findAll(Pageable.unpaged())
//    }
//
//    /**
//     * Finds LeaveRecords by ID: Returns LeaveRecords based on ID
//     * HTTP Code 200: The LeaveRecord object
//     * HTTP Code 404: A object with the submitted ID does not exist!
//     */
//    override suspend fun getById(id: Long): LeaverecordLeaveRecord? {
//        TODO("not checked yet")
//        return repository.findById(id).orElseThrow404(id)
//    }
//
//    /**
//     * Update the LeaveRecord: Updates an existing LeaveRecord
//     * HTTP Code 200: The updated model
//     * HTTP Code 404: The requested object could not be found by the submitted id.
//     * HTTP Code 422: On or many fields contains a invalid value.
//     */
//    override suspend fun update(body: LeaverecordLeaveRecord, id: Long): LeaverecordLeaveRecord {
//        val original = repository.findById(id).orElseThrow404(id)
//        TODO("not checked yet - update the values you really want updated")
//        return repository.save(original)
//    }
    override suspend fun createleaveRecords(body: LeaverecordLeaveRecordDTO, id: Long): LeaverecordLeaveRecordDTO {
        TODO("Not yet implemented")
    }

    override suspend fun getByEmployee(id: Long, maxResults: Int, page: Int): Page<LeaverecordLeaveRecordDTO> {
        TODO("Not yet implemented")
    }

    override suspend fun getById(id: Long): LeaverecordLeaveRecordDTO? {
        TODO("Not yet implemented")
    }

    override suspend fun update(body: LeaverecordLeaveRecordDTO, id: Long): LeaverecordLeaveRecordDTO {
        TODO("Not yet implemented")
    }
}
